# # Bayesian inference
#
# ## Time to capture prey

# ### Load packages

using Distributions
using DataFrames
using CairoMakie
using Turing
import CSV

path_fig = joinpath("figures", "generated") #src

# ### Read data
#
# Simulated data corresponding to the time fur seals need to capture their first prey.

data = CSV.read("data/simulated/time-to-capture.csv", DataFrame) #src
#nb data = CSV.read("../data/simulated/time-to-capture.csv", DataFrame)
n = nrow(data)

# ### Bayesian inference with conjugate posterior

a, b = 2, 30 ## hyperparametros
prior = InverseGamma(a, b)
posterior = InverseGamma(n + a, b + sum(data.time))

# Let's visualise the prior and posterior distribution:

axis_args = (xlabel = "θ", title = "Prior and posterior distributions")
lines(0..70, x -> pdf(prior, x), label = "π(θ)", axis = axis_args)
lines!(0..70, x -> pdf(posterior, x), label = "π(θ ∣ y)")
axislegend()
current_figure()
# savefig(joinpath(path_fig, "03-time-to-capture-naive-posterior.png")) #src

# #### Point estimate

mean(posterior)
#-
var(posterior)

# #### Credible interval

α = 0.05
ci_q = quantile(posterior, [α/2, 1 - α/2])

#-
ch = Chains(rand(posterior, 20000), [:θ])
ci_hpd = DataFrame(hpd(ch))
ci_hpd = vcat(ci_hpd.lower, ci_hpd.upper)

#-

lines(0..70, x -> pdf(posterior, x), label = "π(θ ∣ y)", color = :black)
vlines!(ci_q, label = "Quantile CI", color = :red)
vlines!(ci_hpd, label = "HDP CI", color = :blue)
axislegend()
current_figure()


# #### Comparison with a classical MLE estimator

est = Normal(mean(data.time), sqrt(var(data.time) / 10))
#-

lines(0..70, x -> pdf(posterior, x), label = "π(θ ∣ y)",
    axis = (;title = "Comparison with the MLE")
)
vlines!(ci_q, label = "Quantile CI", color = :red)
lines!(0..70, x -> pdf(est, x), label = "MLE")
vlines!(ci_hpd, label = "HDP CI", color = :blue)
axislegend()
current_figure()

# #### Excedance probability

lines(0..70, x -> pdf(posterior, x), label = "π(θ ∣ y)",
    axis = (; title = "Excedance probability"))
vlines!([30], linestyle = :dash, label = "Threshold", color = :red)
axislegend()
current_figure()
#-
1 - cdf(posterior, 30)

# #### A function of the parameter

# Distribution of a more complex function $log(\theta)$.

hist(log.(rand(posterior, 10000)), normalization = :pdf, bins = 30,
    axis = (xlabel = "log(θ)", ylabel = "posterior", title = "π(log(θ) ∣ y)")
)

# mean of a more complex function

mean(log.(rand(posterior, 10000))) # adequate
#-
log(mean(posterior)) # wrong
#-
quantile(log.(rand(posterior, 10000)), [α/2, 1 - α/2]) # adequate

# #### Predictive distribution

an = n + a
bn = b + sum(data.time)
#-
preds = bn * (rand(100000) .^ (-1/an) .- 1)
hist(preds, normalization = :pdf, bins = 300)
xlims!(low = 0, high = 300)
lines!(0..300, x -> pdf(Exponential(mean(posterior)), x), color = :red)
current_figure()


# ### Using the Turing package

# Define the Bayesian model.

@model function bayes_exponential(y, n)
    ## prior
    θ ~ InverseGamma(2, 30)

    ## likelihood
    for i in 1:n
        y[i] ~ Exponential(θ)
    end
end

# Evaluate the model in the data.

m1 = bayes_exponential(data.time, 10)

# Sample from the posterior distribution using MCMC.

chain = Turing.sample(m1, NUTS(0.65), 10000)

# Visualise traceplot and density of the posterior.

postsamples = vec(get(chain, :θ)[1])
hist(postsamples, bins = 50, normalization = :pdf)

# Visualise prior and posterior densities.

axis_args = (xlabel = "θ", ylabel = "density",
    title = "Comparison of the analytical result and MCMC")
density(postsamples, label = "Turing", axis = axis_args)
lines!(0..100, x -> pdf(prior, x), label = "π(θ)", color = :red)
lines!(0..100, x -> pdf(posterior, x), label = "π(θ∣y)", color = :blue)
axislegend()
current_figure()

