# # Bayesian models
#
# ## Bayesian linear model

# In this section, we will perform Bayesian inference in a linear model:
# \begin{align}
#   Y_i & \sim Normal(\mu_i, sigma^2) \\
#   \mu_i & = x_i^' \beta
# \end{align}

# ### Load packages

using LinearAlgebra
using FillArrays
using Distributions
using DataFrames
using Plots
using Turing
using StatsPlots
import DisplayAs
default(dpi = 120, plot_titlefontsize = 11, titlefontsize = 11)

# ### Simulate data

## dimensions and effects
n, q = 200, 5
β = [1.5, -2, 3, -1, 2]

## predictors
X = randn(n, q)

## response variable
data = DataFrame(X, :auto)
data.y = 3.1 .+ X * β + 2 * randn(n)
data

# Let's visualise the relationship of the predictors with the response variable.
plot(Matrix(data[:, 1:5]), data.y, st = :scatter, layout = 5, ms = 1, msw = -1,
    label = :false, xlab = "x" .* string.(transpose(1:5)), ylab = "y",
    plot_title = "Univariate relationship")
DisplayAs.PNG(current())

# ### Define the Bayesian model in Turing

@model function blm(y, X, p)
    ## prior
    σ² ~ InverseGamma(0.5, 0.5)
    β ~ MvNormal(Zeros(p), 10 * I)

    ## likelihood
    y ~ MvNormal(X * β, σ² * I)
end

# ### Get samples from the posterior distribution

# Evaluate the model in the data.

X = hcat(ones(n), Matrix(data[:, 1:5]))
m1 = blm(data.y, X, size(X, 2))

# Sample from the posterior distribution using MCMC.

chain = Turing.sample(m1, NUTS(0.65), 5000, progress = false)

# Visualise traceplot and density for $\sigma^2$.

plot(chain[[:σ²]], lw = [0.3 0.8], size = (500, 250))
DisplayAs.PNG(current())

# Visualise traceplot and density for $\beta$.

plot(chain[namesingroup(chain, :β)], size = (900, 1200))
DisplayAs.PNG(current())
## corner(chain)

# ### Bayesian estimation

# Remember that we use that posterior distribution to do inference.

# #### Point estimates

# A common point estimation is the expected value of the posterior distribution:
# $E(\theta \mid y)$.

summarize(chain)
#-
mean(chain)

# #### Credible Interval

# We can obtain credible intervals to summarise our results. One easy way to obtain them
# is to compute the quantiles.

α = 0.05
quantile(chain, q = [α/2, 1-α/2])

# #### Excedance probability

# Another common summary that is exceedance probabilities. For example, $Pr(\beta_2 > 1)$.
# Lets start visualising the posterior distribution.

β2pos = vec(chain["β[3]"])
histogram(β2pos, normalize = true, label = false,
    title = "π(β₂∣y)", xlab = "β₂")
DisplayAs.PNG(current())

# Computing exceedance probability.
mean(β2pos .> 1.5)

# #### Functions of the parameters

# We can also obtain the posterior distribution of functions of the parameter. For
# example, $\beta_2 - \beta_3$:

diffpos = chain["β[3]"] - chain["β[4]"] |> vec
histogram(β2pos, normalize = true, label = false,
    title = "π(β₂-β₃∣y)", xlab = "β₂-β₃")
DisplayAs.PNG(current())

# We compute, point estimates, credible interval, so on.

α = 0.05
quantile(diffpos, [α/2, 1-α/2])

# #### Predictive

# Lets obtain the predictive distribution for a $x^*$

## Posterior samples, new profile
B = Array(chain)
xnew = vcat([1], randn(5))
ynew = zeros(size(B, 1))

## Sample predictions using the posterior parameter samples
for i = 1:size(B, 1)
    meannew = xnew' * B[i, Not(1)]
    ynew[i] = Normal(meannew, sqrt(B[i, 1])) |> rand
end
ynew

# Visualize
histogram(ynew, title = "π(y* ∣ y, x*)", label = false)
DisplayAs.PNG(current())

# Summarise prediction
quantile(ynew, [α/2, 1 - α/2])

# Exceedance probability, for example $Pr(Y^* > 0 \mid y)$
mean(ynew .> 0)


# ## Bayesian generalized linera model

# In this section, we will perform Bayesian inference in a generalized linear model:
# \begin{align}
#   Y_i & \sim Exponential(\mu_i) \\
#   \log(\mu_i) & = x_i^' \beta
# \end{align}

# ### Load packages

using LinearAlgebra
using FillArrays
using Distributions
using DataFrames
using Plots
using Turing
using StatsPlots
import CSV
import DisplayAs
default(dpi = 120, titlefontsize = 11)

# ### Read data
#
# Simulated data corresponding to the time fur seals need to capture their first prey.

data = CSV.read("data/simulated/time-to-capture.csv", DataFrame) #src
#nb data = CSV.read("../data/simulated/time-to-capture.csv", DataFrame)
n = nrow(data)

# Visualize

plot(data.age, data.time, st = :scatter, label = false,
    xlab = "age", ylab = "time", title = "Time vs age")

# ### Define the Bayesian model in Turing

@model function glmexp(y, X, n)
    ## prior
    β ~ MvNormal(Zeros(2), 10 * I)

    ## parametros intermedios
    μ = exp.(X * β)

    ## likelihood
    for i in 1:n
        y[i] ~ Exponential(μ[i])
    end
end

# ### Get samples from the posterior distribution

# Evaluate the model in the data.

X = hcat(ones(n), data.age)
m1 = glmexp(data.time, X, size(X, 1))

# Sample from the posterior distribution using MCMC.

chain = Turing.sample(m1, NUTS(0.65), init_params = [0, 0], 10000, progress = false)

# Visualise traceplot and density

plot(chain, lw = [0.1 0.8], size = 1.5 .* (500, 250))
DisplayAs.PNG(current())

# ### Bayesian estimation

# #### Point estimates

summarize(chain)
#-
mean(chain)

# #### Credible Interval

α = 0.1
quantile(chain, q = [α/2, 1-α/2])

# #### Excedance probability

# $Pr(\beta_1 > 0)$

mean(chain["β[2]"] .< 0)

# #### Predictive

# Let's obtain the prediction for different ages:

## Posterior samples and new profiles
B = Array(chain)
xnews = range(extrema(data.age)..., length = 100)
ynews = zeros(length(xnews), size(B, 1))

## Sample predictions
for i = 1:size(B, 1)
    meannew = exp.(B[i, 1] .+ B[i, 2] * xnews)
    ynews[:, i] = rand.(Exponential.(meannew))
end
ynews

# Summarise posterior prediction
postmean = mean(ynews, dims = 2)
α = 0.1
postquant = quantile(Chains(ynews'), q = [α/2, 1 - α/2])[:, 2:3]

# predictive classic
b_mean = mean(B, dims = 1)'
meannew = exp.(b_mean[1] .+ b_mean[2] * xnews)
meanquant = map(x -> quantile.(x, [0.1, 0.9]), Exponential.(meannew))
meanquant = hcat(meanquant...)'

# Visualize

plot(xnews, postquant[:, 1], fillrange = postquant[:, 2], color = :blue, alpha = 0.5,
    xlab = "age", ylab = "time",
    label = "Predictive credible interval")
plot!(xnews, meanquant[:, 1], fillrange = meanquant[:, 2], color = :red, alpha = 0.5,
    label = "Classical interval")
plot!(xnews, postmean, color = :blue, linestyle = :dash,
    label = "Predictive mean")
plot!(data.age, data.time, st = :scatter, msw = -1, color = :black,
    label = "Observations")
DisplayAs.PNG(current())

# ## Bayesian generalized liner mixed model

# In this section, we will perform Bayesian inference in a generalized linear mixed model:
# \begin{align}
#   Y_{ij} & \sim Normal(\mu_{ij}) \\
#   \mu_{ij} & = x_{ij}^' \beta + \alpha_{j}
# \end{align}

# ### Load packages

using LinearAlgebra
using FillArrays
using Distributions
using DataFrames
using Plots
using Turing
using StatsPlots
import CSV
import DisplayAs
default(dpi = 120, plot_titlefontsize = 11, titlefontsize = 11)

# ### Simulate data

## dimensions
n, q = 200, 2
ng = 10
groups = rand(1:ng, n)

## predictors
β = [1.5, -2]
X = randn(n, q)

## group effects
eg = 2 * randn(ng)

## data
data = DataFrame(X, :auto)
data.groups = groups
data.y = X * β + eg[data.groups] + 2 * randn(n)

# ### Visualize

# Univariate relationship.

plot(Matrix(data[:, 1:2]), data.y, st = :scatter, layout = 2,
    ms = 1, msw = -1, label = :false,
    xlab = "x" .* string.(transpose(1:2)), ylab = "y",
    plot_title = "Univariate relationship"
)

# Groups comparison.

plot(data.groups, data.y, st = :boxplot, label = false,
    xlab = "group", ylab = "y",
    plot_title = "Responses by group")

# ### Define the Bayesian model in Turing

@model function glmm(y, X, groups, ng)
    ## prior
    σ² ~ InverseGamma(0.5, 0.5)
    τ² ~ InverseGamma(0.5, 0.5)
    β ~ MvNormal(Zeros(3), 10 * I)

    ## random effect
    α ~ MvNormal(Zeros(ng), τ² * I)

    ## likelihood
    y ~ MvNormal(X * β + α[groups], σ² * I)
end

# ### Get samples from the posterior distribution

# Evaluate the model in the data.
X = hcat(ones(n), Matrix(data[:, 1:2]))
m1 = glmm(data.y, X, data.groups, ng)

# Sample from the posterior distribution using MCMC.

chain = Turing.sample(m1, NUTS(0.65), 5000)

# Visualise traceplot and density of fixed effects

plot(chain[namesingroup(chain, :β)])
DisplayAs.PNG(current())

# Visualise traceplot and density of random effects

plot(chain[namesingroup(chain, :α)[1:5]])
DisplayAs.PNG(current())

#-
#
plot(chain[namesingroup(chain, :α)[6:10]])
DisplayAs.PNG(current())

# ### Bayesian estimation

# #### Point estimates

summarize(chain)
#-
mean(chain)

# #### Credible Interval

α = 0.1
quantile(chain, q = [α/2, 1-α/2])

# #### Excedance probability
# $Pr(\beta_2 > 2)$

mean(chain["α[2]"] .> 0)

# #### Visualize random effects

α = 0.05
postmean = mean(chain[namesingroup(chain, :α)])[:, 2]
postquant = chain[namesingroup(chain, :α)] |>
    x -> quantile(x, q = [α/2, 1-α/2])[:, 2:3]

plot(1:10, postmean, st = :scatter,
    yerror = (postmean - postquant[:, 1], postquant[:, 2] - postmean),
    label = false, xlab = "group", ylab = "effect"
)

