function mergefiles(input, output)
    text = vcat(map(readlines, input)...)
    open(output,"w") do file
        for line in text
            println(file, line)
        end
    end
end

function mergefolder(folder, output)
    input = filter(x -> occursin(r"^[0-9]+.*\.jl$", x), readdir(folder)) |>
        x -> joinpath.(folder, x)
    mergefiles(input, output)
    return output
end

function copyproject(path_from = joinpath("..", "scripts", "code"))
    map(["Manifest.toml", "Project.toml"]) do x
        cp(joinpath(path_from, x), x, force = true)
    end
end

function createscripts(path_from = joinpath("..", "scripts", "code"), path_to = "scripts")
    # create output directory
    rm(path_to, recursive = true, force = true)
    mkpath(path_to)

    # get input and output paths
    folders = readdir(path_from) |>
        z -> filter(x -> occursin(r"^[0-9]+-.*$", x), z)
    path_inputs = joinpath.(path_from, folders)
    path_outputs = joinpath.(path_to, folders .* ".jl")

    # create joined scripts
    mergefolder.(path_inputs, path_outputs)
end

using Literate

function createnotebooks(folder = "scripts")
    # get input and output paths
    jls = filter(
        x -> occursin(r"/[0-9]+-.*\.jl$|/index.jl$", x),
        readdir(folder, join = true)
    )
    ipynbs = replace.(replace.(jls, folder => "notebooks"), ".jl" => ".ipynb")

    # create notebooks
    rm("notebooks", recursive = true, force = true)
    Literate.notebook.(jls, "notebooks", execute = true)
end

# # Create markdown files
# repo_path = "https://github.com/ErickChacon/01-computational-statistics-julia/blob/main"
# rm(joinpath("docs", "src"), recursive = true, force = true)
# Literate.markdown.(jls, joinpath("docs", "src"), execute = true, documenter = true,
#     repo_root_url = repo_path, credit = false)
